package sample;

import java.io.*;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Controller containing functions for the user to login to an existing account, also has functions for changing the
 * view for the user to the create an account page or the menu upon creating a valid account
 */

public class Login {
    public Button loginButton;

    @FXML
    private TextField username, password;


    private User currentUser;
    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }
    /**
     * A method to change to the menu screen if a valid login is given or give an alert that the login details are
     * incorrect
     * @throws IOException
     */
    public void login(ActionEvent event) throws IOException {
        String username_text = username.getText();
        String password_text = password.getText();
        UserDatabase userDatabase = new UserDatabase();
        if (userDatabase.usernameExists(username_text)) {
            User user = userDatabase.getUser(username_text);
            if (user.getPassword().equals(password_text))
            {
                setCurrentUser(user);
                /**
                 * Attempting to load the current user and pass the data here
                 */

                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("menu.fxml"));
                Parent parent = loader.load();
                Menu controller = loader.getController();
                controller.setCurrentUser(currentUser);
                Scene scene = new Scene(parent);
                Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                stage.setScene(scene);
                stage.show();
            }
            else
            {
                invalidLoginAlert("Password");
            }
        }
        else
        {
            invalidLoginAlert("Username");
        }
    }


    /**
     * A method to give the user an alert if their login is incorrect.
     */
    public void invalidLoginAlert(String error) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Kenko");
        alert.setHeaderText("Invalid " + error);
        alert.setContentText("The login details you have entered do not currently exist in our system. Please try again " +
                "or create a new account.");
        alert.showAndWait();
    }

    /**
     * Method to open the screen for the user to create a new account.
     * @throws IOException
     */
    public void openCreateAccount(ActionEvent event) throws IOException {
        Parent parent = FXMLLoader.load(getClass().getResource("createAccount.fxml"));
        Scene scene = new Scene(parent);
        Stage stage =(Stage)((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }
}