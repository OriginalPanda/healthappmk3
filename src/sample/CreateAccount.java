package sample;

import java.io.*;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import java.io.FileWriter;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;

/**
 * Controller containing functions to create an account and change the view for the user
 */

public class CreateAccount {
    @FXML
    private TextField firstName, surname, email, username,
            password, age, startWeight, height, weightGoal, distanceGoal, startDistance, BFP;
    @FXML
    private RadioButton female,male;
    @FXML
    private ToggleGroup sex;
    private Alert alert;

    /**
     * Method to take the new users details and create a new account if the details are all valid.
     * @throws IOException
     */
    public void createAccount(ActionEvent event) throws IOException {
        // Taking the user input from the application and storing it
        String firstName_text = firstName.getText();
        String surname_text = surname.getText();
        String age_text = age.getText();
        String email_text = email.getText();
        String username_text = username.getText();
        String password_text = password.getText();
        String weight_text = startWeight.getText();
        String height_text = height.getText();
        String weightGoal_text = weightGoal.getText();
        String distanceGoal_text = distanceGoal.getText();
        String currDistance_text = startDistance.getText();
        String BFP_text = BFP.getText();
        String female_text = "";
        UserDatabase userDatabase = new UserDatabase();
        RadioButton selected = (RadioButton)sex.getSelectedToggle();
        if (selected == male)
        {
            female_text = "false";
        }
        else if (selected == female)
        {
            female_text = "true";
        }
        //Validation checks
        if (!userDatabase.validUsername(username_text)) {
            invalid("username");
        } else if (!userDatabase.validPassword(password_text)) {
            invalid("password");
        } else if (!userDatabase.validEmail(email_text)) {
            invalid("email");
        } else if (!weight_text.matches("[0-9]{1,}")) {
            invalid("weight");
        } else if (!age_text.matches("[0-9]{1,}")) {
            invalid("age");
        } else if (!height_text.matches("[0-9]{1,}")) {
            invalid("height");
        } else if (!weightGoal_text.matches("[0-9]{1,}")) {
            invalid("Weight Goal");
        } else if (!distanceGoal_text.matches("[0-9]{1,}")) {
            invalid("Distance Goal");
        } else if (!currDistance_text.matches("[0-9]{1,}")) {
            invalid("Current distance");
        } else if (!BFP_text.matches("[0-9]{1,}")) {
        invalid("Body Fat Percentage");
    }
        //If the data passes the validation checks it is written to files.
        else {
            try {
                User user = new User(firstName_text, surname_text, email_text, username_text, password_text,
                        Integer.parseInt(age_text), Double.parseDouble(weight_text), Double.parseDouble(height_text),
                        Double.parseDouble(weightGoal_text), Double.parseDouble(distanceGoal_text),
                        Double.parseDouble(currDistance_text), Double.parseDouble(BFP_text), female_text);
                user.saveFirstDistanceProgress();
                user.saveFirstWeightProgress();
                userDatabase.addUser(user);
                FileWriter fw = new FileWriter("userDatabase.csv", true);
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write(user.toString());
                bw.write("\n");
                bw.flush();
                bw.close();
                // Open successfully created popup
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Kenko");
                alert.setHeaderText("WELCOME TO THE KENKO FAMILY!!! :)");
                alert.setContentText("Successfully created an account.");
                alert.showAndWait();
                /**
                 * After creating an account we should just move the user through to the main menu page
                 */

                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("menu.fxml"));
                Parent parent = loader.load();
                Menu controller = loader.getController();
                controller.setCurrentUser(user);
                Scene scene = new Scene(parent);
                Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                stage.setScene(scene);
                stage.show();
//                FXMLLoader loader = new FXMLLoader();
//                loader.setLocation(getClass().getResource("menu.fxml"));
//                Parent parent = loader.load();
//                Menu controller = loader.getController();
//                controller.setCurrentUser(user);
//                Stage newAccStage = new Stage();
//                Scene scene = new Scene(parent);
//                newAccStage.setTitle("Kenko");
//                newAccStage.setScene(scene);
//                newAccStage.show();

            }catch (IllegalArgumentException e)
            {
                alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Kenko");
                alert.setHeaderText("Error");
                alert.setContentText(e.toString());
                alert.showAndWait();
            }
        }
    }

    /**
     * A method to give the user an alert if their data is of the incorrect format.
     */
    public void invalid(String error) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Kenko");
        alert.setHeaderText("Invalid " + error);
        alert.setContentText("Please try again");
        alert.showAndWait();
    }

    /**
     * Method to move the user to the main menu page upon creating a valid account
     * @param event
     * @throws IOException
     */
    public void openMenu(ActionEvent event) throws IOException {
        Parent parent = FXMLLoader.load(getClass().getResource("menu.fxml"));
        Scene scene = new Scene(parent);
        Stage stage =(Stage)((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Method to move the user to the main menu page upon creating a valid account
     * @param event
     * @throws IOException
     */
    public void openLogin(ActionEvent event) throws IOException {
        Parent parent = FXMLLoader.load(getClass().getResource("login.fxml"));
        Scene scene = new Scene(parent);
        Stage stage =(Stage)((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }
}
