package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class DietPlanner {
    @FXML
    private RadioButton veryLight, light, moderate, heavy, veryHeavy, one, two, three, four;

    @FXML
    ToggleGroup exercise, change, percentage;

    Double weight, BFP;

    String female;


    User currentUser;

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
        this.weight = currentUser.getStartWeight();
        this.BFP = currentUser.getBFP();
        this.female = currentUser.getFemale();
    }

    public double calculateBMR(){ // BFP = body fat percentage

        Boolean place = null;

        female = female.toLowerCase();
        if(female.equals("true")){
            place = true;
        }
        else if(female.equals("false")){
            place =false;
        }

         if(place){
            weight = weight * 0.9;
        }

        double BMR = weight * 24 ;

        if(BFP < 14 && !place){

        }
        else if(BFP >= 14 && BFP <21 && !place){
            BMR *= 0.95;
        }
        else if(BFP >= 20 && BFP <29  && !place){
            BMR *= 0.9;
        }
        else if(!place && BFP < 14){
            BMR *= 0.85;
        }
        else if(BFP >= 14 && BFP < 19){

        }
        else if (BFP >= 19 && BFP < 29){
            BMR *= 0.95;
        }
        else if(BFP >=29 && BFP < 38){
            BMR *=0.9;
        }
        else if(BFP >=38){
            BMR *= 0.85;
        }

        RadioButton selected = (RadioButton)exercise.getSelectedToggle();

        if(selected == veryLight){
            BMR *= 1.3;
        }
        else if(selected == light){
            BMR *= 1.55;
        }
        else if(selected == moderate){
            BMR *= 1.65;
        }
        else if(selected == heavy){
            BMR *= 1.8;
        }
        else if(selected == veryHeavy){
            BMR *= 2;
        }

        return BMR;


    }

    public void showResult(double result){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Kenko");
        alert.setHeaderText("Recommended Caloric Intake : ");
        alert.setContentText("" + result);
        alert.showAndWait();
    }


    public void gain(){
        double BMR = calculateBMR();
        double result =  BMR * (1 + calcPercentage());
        showResult(result);
    }

    public void maintain(){
        double BMR = calculateBMR();
        showResult(BMR);
    }

    public void lose(){
        double BMR = calculateBMR();
        double result =  BMR * (1 - calcPercentage());
        showResult(result);
    }
    public double calcPercentage(){
        RadioButton selected = (RadioButton)percentage.getSelectedToggle();
        if(selected == one){
            return .01;
        }
        else if(selected == two){
            return .02;
        }
        else if(selected == three){
            return .03;
        }
        else if(selected == four){
            return .04;
        }
        return .0;
    }


    public void openMenu(ActionEvent event) throws IOException{
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("menu.fxml"));
        Parent parent = loader.load();
        Menu controller = loader.getController();
        controller.setCurrentUser(currentUser);
        Scene scene = new Scene(parent);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }
}



