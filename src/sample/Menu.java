package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

/**
 * Controller for the main menu page, contains functions to change the view for the user to various pages upon
 * selection
 */
public class Menu {
    User currentUser;

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    /**
     * Method to open the progress scene
     * @throws IOException
     */

    public void openProgressMenu(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("ProgressMenu.fxml"));
        Parent parent = loader.load();
        ProgressMenu controller = loader.getController();
        controller.setCurrentUser(currentUser);
        changeScene(event, parent);
    }

    /**
     * Method to open the workout scene
     * @throws IOException
     */
    public void openWorkout(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("workout.fxml"));
        Parent parent = loader.load();
        Workout controller = loader.getController();
        controller.setCurrentUser(currentUser);
        changeScene(event, parent);
    }

    public void openDiet(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("dietPlanner.fxml"));
        Parent parent = loader.load();
        DietPlanner controller = loader.getController();
        controller.setCurrentUser(currentUser);
        changeScene(event, parent);
    }

    public void changeScene(ActionEvent event, Parent parent){
        Scene scene = new Scene(parent);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }
}
