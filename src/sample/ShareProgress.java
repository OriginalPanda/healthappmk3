package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
//import javax.activation.*;

import java.awt.*;

/**
 * Controller containing methods to allow the user to be able to share their progress with their friends (requires an
 * email of their friend and their email login). Also contains a method for returning the user to the main menu
 * page.
 */
public class ShareProgress {

    @FXML TextField EmailFriend, NameFriend, EmailPassword;

    User currentUser;
     public void setCurrentUser(User currentUser){
         this.currentUser = currentUser;
     }

    /**
     * Method to send an email out to a given email address giving the user's details and progression
     * @throws MessagingException
     */
    @FXML
    public void Send_email () throws MessagingException {

        //Getting user details from object
        String from = currentUser.getEmail();
        currentUser.loadDistanceProgress();
        currentUser.loadWeightProgress();
        ArrayList WeightProgress = currentUser.getWeightProgression();
        ArrayList DistanceProgress = currentUser.getDistanceProgression();
        double DistanceGoal = currentUser.getDistanceGoal();
        double WeightGoal = currentUser.getWeightGoal();
        String to = EmailFriend.getText();
        String nameFriend = NameFriend.getText();
        String password = EmailPassword.getText();


        //String from = "josephmendozawork123@gmail.com";

        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");

        //String password = "securepassword123";
        Session session = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        });
        Message message = new MimeMessage(session);
        try {
            message.setFrom(new InternetAddress(from));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject("My Progress");
            message.setText("Hey " + nameFriend + " my goal weight is " + WeightGoal + ", my weight progression is "
                    + WeightProgress +  ", my distance goal is " + DistanceGoal +  " and my distance progression is "
                    + DistanceProgress  );

        } catch (MessagingException e) {
            e.printStackTrace();
        }

        Transport.send(message);
        System.out.println("Email Successfully Sent");
    }


    /**
     * Method to return the user back to the main menu page
     * @param event
     * @throws IOException
     */
    public void openMenu(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("menu.fxml"));
        Parent parent = loader.load();
        Menu controller = loader.getController();
        controller.setCurrentUser(currentUser);
        Scene scene = new Scene(parent);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }
}
