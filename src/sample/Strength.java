package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class Strength {
    User currentUser;

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }



    public void openWorkout(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("workout.fxml"));
        Parent parent = loader.load();
        Workout controller = loader.getController();
        controller.setCurrentUser(currentUser);
        changeScene(event, parent);
    }
    public void changeScene(ActionEvent event, Parent parent){
        Scene scene = new Scene(parent);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    public void strengthDef() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Kenko");
        alert.setHeaderText("WHAT IS STRENGTH");
        alert.setContentText("Physical strength is known as the \n" +
                " measure of an exertion of force on physical objects.");
        alert.showAndWait();
    }
    public void strengthIMP() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Kenko");
        alert.setHeaderText("WAYS TO IMPROVE STRENGTH");
        alert.setContentText("The main ways to improve strength are \n" +
                " lifting heavier weights \n " +
                " and training specifically for strength");
        alert.showAndWait();
    }
    public void strengthTip1() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Kenko");
        alert.setHeaderText("WAYS TO IMPROVE LONG DISTANCE TRAINING");
        alert.setContentText("In terms of strength training there are specific aspects you can train for\n" +
                " muscular endurance - 15 - 30 repetitions of any given exercise\n" +
                " hypertrophy - 8 - 15 repetitions of any given exercise\n" +
                " strength - 3 - 8 repetitions of any given exercise\n" +
                " training within the 3-8 repetition zone will lead to\n" +
                " the greatest increase in strength.");
        alert.showAndWait();
    }
    public void strengthTip2() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Kenko");
        alert.setHeaderText("WAYS TO IMPROVE INTERVAL TRAINING");
        alert.setContentText("The heavier the weight you lift, the more resistance that is\n"+
                " working against you while you lift meaning more of the \n" +
                " type II muscle fibers responsible for generating\n "+
                " muscle force will be involved.");
        alert.showAndWait();
    }
    public void generalTipsStrength() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Kenko");
        alert.setHeaderText("GENERAL TIPS");
        alert.setContentText("With each week that passes,\n" +
                "attempt to increase the weight you are lifting, safely,\n" +
                "to involved progressing even more and avoid strength plateaus,\n" +
                " ensure you're not over-training by making sure\n" +
                "you're not pushing yourself too hardly and getting enough rest,\n +" +
                "this can prevent injury meaning you'll be able to train more often.");
        alert.showAndWait();
    }

}
