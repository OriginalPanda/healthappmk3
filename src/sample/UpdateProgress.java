package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import java.io.*;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Controller containing methods for the user to be able to update their weight/distance progressions
 */
public class UpdateProgress {

    @FXML
    private TextField w2, d2;
    private User currentUser;

    /**
     * Error message for if the user enters the wrong data type
    // * @param data - the field that is incorrect
     **/

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public void invalidDataType(String data) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Kenko");
        alert.setHeaderText("Invalid " + data);
        alert.setContentText("Please try again"+ "\n" + "If you don't have a progress for this please enter a 0.");
        alert.showAndWait();
    }

    /**
     * A method to update the user's progress if the data all formatted correctly
     * @throws IOException
     */
    public void update() throws IOException {
        String weightText = w2.getText();
        String distanceText = d2.getText();
        if (!weightText.matches("[0-9]{1,}")) {
            invalidDataType("Weight Goal");
        }
        else  if(!distanceText.matches("[0-9]{1,}")) {
            invalidDataType("Distance Goal");
        }
        else{
            updateWeights(weightText);
            updateDistance(distanceText);
            confirmationMessage();
        }
    }

    /**
     * Method to confirm to the user that their data has been updated.
     */
    public void confirmationMessage() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Kenko");
        alert.setHeaderText("Successfully updated");
        alert.setContentText("Your progress has been updated!" +"\n"+ "Keep up the good work warrior.");
        alert.showAndWait();
    }


    /**
     * A method to update the user's weight and save the data
     * @param weight - the user weight to update the file with
     */
    public void updateWeights(String weight) throws IOException {
            currentUser.loadWeightProgress();
            currentUser.addNewWeightProgression(Double.parseDouble(weight));
            currentUser.saveWeightProgress();
    }

    /**
     * A method to update the user's distance and save the data
     * @param distance - the user distance to update the file with
     */
    public void updateDistance(String distance) {
        currentUser.loadDistanceProgress();
        currentUser.addNewDistanceProgression(Double.parseDouble(distance));
        currentUser.saveDistanceProgress();
    }


    public void openMenu(ActionEvent event) throws IOException{
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("menu.fxml"));
        Parent parent = loader.load();
        Menu controller = loader.getController();
        controller.setCurrentUser(currentUser);
        Scene scene = new Scene(parent);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }
}
