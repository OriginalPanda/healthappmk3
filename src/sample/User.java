// This class will store information about the user such as;
//  name,age,height startWeight and will calculate the BMI
package sample;

import java.io.*;
import java.util.ArrayList;

/**
 * Class to hold information about the user, and has functions for user operations
 * such as populating the users details from csvs, calculation the bmi and more.
 */
public class User {
    private String firstname;
    private String surname;
    private String email;
    private String userName;
    private String password;
    private String female;
    private double BFP;
    private int age;
    // Weight to be stored as Kg
    private double startWeight;
    //Height to be measured as meters
    private double height;
    // Weight to be stored as Kg
    private double weightGoal;
    //Height to be measured as meters
    private double distanceGoal;
    private double startDistance;
    private ArrayList<Double> weightProgression;
    private ArrayList<Double> distanceProgression;

    /**
     * Method to return true or false dependant on if the user has achieved the weight goal they set out to achieve
     *
     * @return bool
     */
    public boolean achievedWeightGoalGoal() {
        // If users goal was to gain weight
        if (weightGoal > startWeight) {
            if (getMostRecentWeight() >= weightGoal) {
                return true;
            } else {
                return false;
            }
        } else {
            // If users goal was to lose weight
            if (getMostRecentWeight() >= weightGoal) {
                return false;
            } else {
                return true;
            }
        }
    }

    /**
     * Method to return true or false dependant on if the user has achieved the distance goal they set out to achieve
     *
     * @return bool
     */
    public boolean achievedDistanceGoal() {
        if (getMostRecentDistance() >= distanceGoal) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Method to calculate the user's BMI
     *
     * @return
     */
    public double BMI() {
        return startWeight / (height * height);
    }


    /**
     * Constructor for the User using all the information that would be passed in through the create account
     *
     * @param firstname
     * @param surname
     * @param email
     * @param userName
     * @param password
     * @param age
     * @param startWeight
     * @param height
     * @param weightGoal
     * @param distanceGoal
     * @param startDistance
     * @param BFP
     * @param female
     */
    public User(String firstname, String surname, String email, String userName, String password, int age,
                double startWeight, double height, double weightGoal, double distanceGoal, double startDistance,
                double BFP, String female) {
        this.firstname = firstname;
        this.surname = surname;
        this.email = email;
        this.userName = userName;
        this.password = password;
        this.age = age;
        this.startWeight = startWeight;
        this.height = height;
        this.weightGoal = weightGoal;
        this.distanceGoal = distanceGoal;
        this.startDistance = startDistance;
        this.BFP = BFP;
        this.female = female;
    }

    @Override
    public String toString() {
        return firstname + "," + surname + "," + email + "," + userName + "," + password + "," + age + "," + startWeight + "," +
                height + "," + weightGoal + "," + distanceGoal + "," + startDistance + "," + BFP + "," + female;
    }

    public User() {
    }

    public void addNewWeightProgression(Double weight) {
        weightProgression.add(weight);
    }

    public void addNewDistanceProgression(Double distance) {
        distanceProgression.add(distance);
    }


    /**
     * Initialises the weight progression for the logged in user from the CSV containing their tracked weights
     */
    public void loadWeightProgress() {
        BufferedReader bufferedReader = null;
        String fileName = "weights.csv";
        String line = "";
        String csvSplitBy = ",";
        weightProgression = new ArrayList<>();
        try {
            bufferedReader = new BufferedReader(new FileReader(fileName));
            while ((line = bufferedReader.readLine()) != null) {
                String[] split = line.split(csvSplitBy);
                String username = split[0];
                if (username.equals(getUserName())) {

                    for (int i = 1; i < split.length; i++) {
                        double weight = Double.parseDouble(split[i]);
                        weightProgression.add(weight);
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
    }

    /**
     * Initialises the weight progression for the logged in user and writes their username and start weight to the CSV
     * file
     */
    public void saveFirstWeightProgress() {
        String fileName = "weights.csv";
        try {
            FileWriter fw = new FileWriter((fileName), true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(getUserName() + "," + getStartWeight());
            bw.write("\n");
            bw.flush();
            bw.close();


        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Initialises the distance progression for the logged in user and writes their username and start distance to the
     * CSV file
     */
    public void saveFirstDistanceProgress() {
        String fileName = "distances.csv";
        try {
            FileWriter fw = new FileWriter((fileName), true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(getUserName() + "," + getStartDistance());
            bw.write("\n");
            bw.flush();
            bw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Saves the weight progression for the logged in user to the CSV containing their tracked weights
     */
    public void saveWeightProgress() {
        BufferedReader bufferedReader = null;
        String fileName = "weights.csv";
        String line = "";
        String csvSplitBy = ",";
        String oldText = "";
        String newText = "";
        try {
            bufferedReader = new BufferedReader(new FileReader(fileName));
            while ((line = bufferedReader.readLine()) != null) {

                oldText = oldText + line + "\r\n";
                String[] name = line.split(csvSplitBy);
                String username = name[0];
                // If the line holds data for the current user
                if (username.equals(getUserName())) {
                    String weight = weightProgression.toString();
                    weight = weight.substring(1, weight.length() - 1);
                    newText = oldText.replace(line, username + "," + weight);
                }
                // If the line is other user data
                else {
                    newText = newText + line + "\n";
                }
            }
            // Write the new data back to the csv file
            FileWriter writer = new FileWriter(fileName);
            writer.write(newText);
            writer.close();
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
    }


    /**
     * Initialises the distance progression for the logged in user from the CSV containing their tracked distances
     */
    public void loadDistanceProgress() {
        BufferedReader bufferedReader = null;
        String fileName = "distances.csv";
        String line = "";
        String csvSplitBy = ",";
        distanceProgression = new ArrayList<>();
        try {
            bufferedReader = new BufferedReader(new FileReader(fileName));
            while ((line = bufferedReader.readLine()) != null) {
                String[] split = line.split(csvSplitBy);
                String username = split[0];
                if (username.equals(getUserName())) {
                    for (int i = 1; i < split.length; i++) {
                        double distance = Double.parseDouble(split[i]);
                        distanceProgression.add(distance);
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
    }

    /**
     * Saves the distance progression for the logged in user to the CSV containing their tracked distances
     */
    public void saveDistanceProgress() {
        BufferedReader bufferedReader = null;
        String fileName = "distances.csv";
        String line = "";
        String csvSplitBy = ",";
        String oldText = "";
        String newText = "";
        try {
            bufferedReader = new BufferedReader(new FileReader(fileName));
            while ((line = bufferedReader.readLine()) != null) {
                oldText = oldText + line + "\r\n";
                String[] name = line.split(csvSplitBy);
                String username = name[0];
                // If the line holds data for the current user
                if (username.equals(getUserName())) {
                    String distance = distanceProgression.toString();
                    distance = distance.substring(1, distance.length() - 1);
                    newText = oldText.replace(line, username + "," + distance);
                }
                // If the line is other user data
                else {
                    newText = newText + line + "\n";
                }
            }
            // Write the new data back to the csv file
            FileWriter writer = new FileWriter(fileName);
            writer.write(newText);
            writer.close();
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
    }

    /**
     * Saves the user with updated details to the userDatabase
     */
    public void saveNewUserDetails() {
        BufferedReader bufferedReader = null;
        String fileName = "userDatabase.csv";
        String line = "";
        String csvSplitBy = ",";
        String oldText = "";
        String newText = "";
        try {
            bufferedReader = new BufferedReader(new FileReader(fileName));
            while ((line = bufferedReader.readLine()) != null) {

                oldText = oldText + line + "\r\n";
                String[] name = line.split(csvSplitBy);
                String username = name[4];
                // If the line holds data for the current user
                if (username.equals(getUserName())) {
                    newText = oldText.replace(line, toString());
                }
                // If the line is other user data
                else {
                    newText = newText + line + "\n";
                }
            }
            // Write the new data back to the csv file
            FileWriter writer = new FileWriter(fileName);
            writer.write(newText);
            writer.close();
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
    }

    /**
     * Bellow this is all the getter and setter methods for the user class
     */

    public String getFirstname() {
        return firstname;
    }
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
    public double getWeightGoal() {
        return weightGoal;
    }
    public void setWeightGoal(double weightGoal) {
        this.weightGoal = weightGoal;
    }
    public double getDistanceGoal() {
        return distanceGoal;
    }
    public void setDistanceGoal(double distanceGoal) {
        this.distanceGoal = distanceGoal;
    }
    public double getStartDistance() {
        return startDistance;
    }
    public void setStartDistance(double startDistance) {
        this.startDistance = startDistance;
    }
    public ArrayList<Double> getWeightProgression() {
        return weightProgression;
    }
    public Double getMostRecentWeight() {
        return weightProgression.get(weightProgression.size() - 1);
    }

    public Double getMostRecentDistance() {
        return distanceProgression.get(distanceProgression.size() - 1);
    }

    public void setWeightProgression(ArrayList<Double> weightProgression) {
        this.weightProgression = weightProgression;
    }

    public ArrayList<Double> getDistanceProgression() {
        return distanceProgression;
    }

    public void setDistanceProgression(ArrayList<Double> distanceProgression) {
        this.distanceProgression = distanceProgression;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getBFP() {
        return BFP;
    }

    public void setBFP(double BFP) {
        this.BFP = BFP;
    }

    public String getFemale() {
        return female;
    }

    public void setFemale(String female) {
        this.female = female;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public String getUserName() {
        return userName;
    }

    public String getName() {
        return firstname;
    }

    public double getHeight() {
        return height;
    }

    public double getStartWeight() {
        return startWeight;
    }

    public int getAge() {
        return age;
    }

    public void setName(String firstname) {
        this.firstname = firstname;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setStartWeight(double startWeight) {
        this.startWeight = startWeight;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
