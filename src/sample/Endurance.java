package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class Endurance {
    User currentUser;

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public void openWorkout(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("workout.fxml"));
        Parent parent = loader.load();
        Workout controller = loader.getController();
        controller.setCurrentUser(currentUser);
        changeScene(event, parent);
    }
    public void changeScene(ActionEvent event, Parent parent){
        Scene scene = new Scene(parent);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    public void enduranceDef() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Kenko");
        alert.setHeaderText("WHAT IS ENDURANCE");
        alert.setContentText("Endurance requires the circulatory and respiratory systems to\n" +
                " supply energy to the working muscles in order to support sustained\n" +
                " physical activity.\n ");
        alert.showAndWait();
    }
    public void enduranceIMP() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Kenko");
        alert.setHeaderText("WAYS TO IMPROVE ENDURANCE");
        alert.setContentText("The main ways to improve endurance are long, \n" +
                " moderate pace, distance training \n " +
                " and interval training");
        alert.showAndWait();
    }
    public void enduranceTip1() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Kenko");
        alert.setHeaderText("WAYS TO IMPROVE LONG DISTANCE TRAINING");
        alert.setContentText("This is the most basic/ beginner friendly method to\n" +
                " improve physical endurance. Jog at a consistent pace which\n" +
                " you can handle for 20-30 minutes without stopping 3-4 times\n" +
                " a day. This can be done with any cardio machine");
        alert.showAndWait();
    }
    public void enduranceTip2() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Kenko");
        alert.setHeaderText("WAYS TO IMPROVE INTERVAL TRAINING");
        alert.setContentText("Training which consists of short, repeated, intense sessions.\n" +
                " sessions normally last around 10-15 minutes. Intervals last around\n" +
                " 30 seconds with 1 minute of rest, continue till desired session duration.\n" +
                " This can be done with any cardio machine");
        alert.showAndWait();
    }
    public void generalTipsEndurance() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Kenko");
        alert.setHeaderText("GENERAL TIPS");
        alert.setContentText("With each week that passes, attempt to increase the duration\n" +
                " for either long distance training or interval training,\n" +
                " ensure you're not over-training by limiting the amount of days\n" +
                " you train to 3-4, this can prevent injury and lead to\n" +
                " adequate recovery and to also switch between\n" +
                " the 2 types of training methods for more variety");
        alert.showAndWait();
    }
}
