package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class Flexibility {
    User currentUser;

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public void openWorkout(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("workout.fxml"));
        Parent parent = loader.load();
        Workout controller = loader.getController();
        controller.setCurrentUser(currentUser);
        changeScene(event, parent);
    }
    public void changeScene(ActionEvent event, Parent parent){
        Scene scene = new Scene(parent);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }


    public void flexibilityDef() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Kenko");
        alert.setHeaderText("WHAT IS FLEXIBILITY/MOBILITY");
        alert.setContentText("Flexibility is known as the ability of a muscle\n" +
                " to lengthen passively through a range of motion.\n" +
                " Mobility is known as the ability of a joint\n" +
                " to move actively through a range of motion.");
        alert.showAndWait();
    }
    public void flexibilityIMP() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Kenko");
        alert.setHeaderText("WAYS TO IMPROVE FLEXIBILITY/MOBILITY ");
        alert.setContentText("The main ways to improve flexibility/mobility is\n" +
                " stretching daily if possible if not\n" +
                " then every other day");
        alert.showAndWait();
    }
    public void flexibilityTip1() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Kenko");
        alert.setHeaderText("STRETCH DAILY");
        alert.setContentText("Stretch daily if possible, for around 10-30 minutes\n" +
                " Using a foam roller - can be known as a self massage\n" +
                " Example stretches include;\n" +
                " cobra pose\n"
                + " cat camel\n"
                + " pigeon pose\n"
                + " touch toes\n"
                + " child's pose\n"
                + " rock backs and many more.");
        alert.showAndWait();
    }
    public void generalTipsFlexibility() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Kenko");
        alert.setHeaderText("GENERAL TIPS");
        alert.setContentText("With each week that passes,\n" +
                " attempt to increase the amount of stretching you do\n" +
                " to improve flexibility and mobility,\n" +
                " ensure stretching is dynamic and passive,\n" +
                " try to breathe through the stretches\n" +
                " to make them feel more comfortable.");
        alert.showAndWait();
    }

}
