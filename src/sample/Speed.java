package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class Speed {
    User currentUser;

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public void openWorkout(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("workout.fxml"));
        Parent parent = loader.load();
        Workout controller = loader.getController();
        controller.setCurrentUser(currentUser);
        changeScene(event, parent);
    }
    public void changeScene(ActionEvent event, Parent parent){
        Scene scene = new Scene(parent);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    public void speedDef() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Kenko");
        alert.setHeaderText("WHAT IS SPEED");
        alert.setContentText("Speed is known as the ability to\n" +
                " move quickly across the ground or move limbs rapidly\n" +
                " to grab or throw objects.");
        alert.showAndWait();
    }
    public void speedIMP() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Kenko");
        alert.setHeaderText("WAYS TO IMPROVE SPEED ");
        alert.setContentText("The main ways to improve speed are \n" +
                " building explosive strength mainly in the legs \n " +
                " improving running technique and\n" +
                " improving ankle strength/mobility");
        alert.showAndWait();
    }
    public void speedTip1() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Kenko");
        alert.setHeaderText("WAYS TO IMPROVE EXPLOSIVE (LEG) STRENGTH");
        alert.setContentText("Mainly work on the hamstrings as that's where \n" +
                " the power comes from, practicing many explosive type exercises\n" +
                " such as jumping squats, jumping lunges, box jumps etc.");
        alert.showAndWait();
    }
    public void speedTip2() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Kenko");
        alert.setHeaderText("WAYS TO IMPROVE RUNNING TECHNIQUE");
        alert.setContentText("Practice running often but instead of just \n" +
                " mindless running, keep track of your form, potentially\n" +
                " if you can record yourself so you can see how you're doing,\n" +
                " aim to run with longer stride lengths and higher knees which\n" +
                " both lead to increases in speed");
        alert.showAndWait();
    }
    public void speedTip3() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Kenko");
        alert.setHeaderText("WAYS TO IMPROVE ANKLE MOBILITY");
        alert.setContentText("Increase ankle strength/mobility,\n"  +
                " this can also lead to improvements in speed\n" +
                " stretches that can improve ankle mobility are\n" +
                " ankle circles\n"
                + " heel lifts\n"
                + " heal drops\n"
                + " lunges\n"
                + " squats and many more");
        alert.showAndWait();
    }
    public void generalTipsSpeed() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Kenko");
        alert.setHeaderText("GENERAL TIPS");
        alert.setContentText("With each week that passes,\n" +
                " attempt to increase the weight you are lifting, safely,\n" +
                " to involved progressing even more and avoid speed plateaus,\n" +
                " ensure you're not over-training by making sure\n" +
                " you're not pushing yourself too hardly and getting enough rest,\n" +
                " this can prevent injury meaning you'll be able to train more often.\n" +
                " attempt to stretch ankles after working out for convenience.");
        alert.showAndWait();
    }

}
