package sample;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

/**
 * Object to hold multiple Users in a database.
 * Contains functions to check the creation of new users based on existing users already in the database.
 */
public class UserDatabase extends User
{
    private ArrayList<User> userArrayList;

    /**
     * Reads from CSV file and initialises all the Users
     */
    public UserDatabase()
    {
        BufferedReader bufferedReader = null;
        String fileName =  "userDatabase.csv";
        try {
            userArrayList = new ArrayList<>();
            bufferedReader = new BufferedReader(new FileReader(fileName));
            String line = "";
            while ((line= bufferedReader.readLine()) != null)
            {
                String [] tokens = line.split(",");
                /**
                 *
                 *public User(String firstname, String surname, String email, String userName, String password,
                 *                 int age, double weight, double height, double weightGoal, double distanceGoal, double currDistance)
                 *
                 private TextField firstName, surname, email, username,
                 password, age, weight, height, weightGoal, distanceGoal, currDistance;
                 */

                    String firstName = tokens[0];
                    String surname = tokens[1];
                    String email = tokens[2];
                    String userName = tokens[3];
                    String password = tokens[4];
                    int age = Integer.parseInt(tokens[5]);
                    double weight = Double.parseDouble(tokens[6]);
                    double height = Double.parseDouble(tokens[7]);
                    double weightGoal = Double.parseDouble(tokens[8]);
                    double distanceGoal = Double.parseDouble(tokens[9]);
                    double currDistance = Double.parseDouble(tokens[10]);
                    double BFP = Double.parseDouble(tokens[11]);
                    String female = tokens[12];
                    User user = new User(firstName,surname,email,userName,password,
                            age,weight,height,weightGoal,distanceGoal,currDistance,BFP, female);
                    userArrayList.add(user);
            }
        }
        catch (Exception e)
        {
            System.out.println("Error: " + e);
        }
    }

    /**
     * Returns user object that has the given username
     * @param username
     * @return
     */
    public User getUser(String username)
    {
        for (User user : userArrayList)
        {
            if ((user.getUserName().equals(username)))
            {
                return user;
            }
        }
        return null;
    }


    // Checks to see in the csv if there already is a user with the given username.
    public boolean usernameExists(String username)
    {
        for (User user :userArrayList)
        {
            if (user.getUserName().equals(username))
            {
                return true;
            }
        }
        return false;
    }

    // Checks to see in the csv if there already is a user with the given email.
    public boolean emailExists(String email)
    {
        for (User user : userArrayList)
        {
            if (user.getEmail().equals(email))
            {
                return true;
            }
        }
        return false;
    }


    // Method that checks that the username is longer than 3 characters
    // and can only contain a-z, A-Z, 0-9, points, dashes and underscores.
    public boolean validUsername(String username)
    {
        if (username.matches("[a-zA-Z0-9.\\-_]{3,}") && !(usernameExists(username)))
        {
            return true;
        }
        return false;
    }


    /**
     * Method to check the email is valid (it doesn't already exist and it fulfills the regex)
     *
     * @param checkEmail - entered email
     * @return bool
     */
    public boolean validEmail(String checkEmail) {
        if (emailExists(checkEmail)) {
            return false;
        }
        if (!checkEmail.matches("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}" +
                "[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")) {
            return false;
        }
        return true;
    }


    /**
     * Method that checks that the password is between 8-20 chars, must contain one uppercase,
     * one lowercase and one special char
     *
     * @param password - entered password
     * @return bool
     */
    public boolean validPassword(String password) {
        if (password.matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*[#$^+=!*()@%&]).{8,20}$")) {
            return true;
        }
        return false;
    }

    // Method to add a user to the database only if the username is valid
    public void addUser(User user)
    {
        userArrayList.add(user);
    }

}
