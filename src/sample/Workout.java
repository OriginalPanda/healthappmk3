package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

import java.awt.*;
import java.io.IOException;
import java.util.Random;

public class Workout {

    User currentUser;

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public void openMenu(ActionEvent event) throws IOException {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("menu.fxml"));
            Parent parent = loader.load();
            Menu controller = loader.getController();
            controller.setCurrentUser(currentUser);
            changeScene(event, parent);
        }
    public void openWorkout(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("workout.fxml"));
        Parent parent = loader.load();
        Workout controller = loader.getController();
        controller.setCurrentUser(currentUser);
        changeScene(event, parent);
    }
    public void openEndurance(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("endurance.fxml"));
        Parent parent = loader.load();
        Endurance controller = loader.getController();
        controller.setCurrentUser(currentUser);
        changeScene(event, parent);
    }
    public void openStrength(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("strength.fxml"));
        Parent parent = loader.load();
        Strength controller = loader.getController();
        controller.setCurrentUser(currentUser);
        changeScene(event, parent);
    }
    public void openFlexibility(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("flexibility.fxml"));
        Parent parent = loader.load();
        Flexibility controller = loader.getController();
        controller.setCurrentUser(currentUser);
        changeScene(event, parent);
    }
    public void openSpeed(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("speed.fxml"));
        Parent parent = loader.load();
        Speed controller = loader.getController();
        controller.setCurrentUser(currentUser);
        changeScene(event, parent);
    }
//    public void openJump(ActionEvent event) throws IOException {
//        FXMLLoader loader = new FXMLLoader();
//        loader.setLocation(getClass().getResource("jump.fxml"));
//        Parent parent = loader.load();
//        Jump controller = loader.getController();
//        controller.setCurrentUser(currentUser);
//        changeScene(event, parent);
//    }

    public void changeScene(ActionEvent event, Parent parent){
        Scene scene = new Scene(parent);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }
}
