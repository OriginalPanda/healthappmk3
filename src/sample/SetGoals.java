package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.awt.*;
import java.io.*;

/**
 * Controller containing methods to allow the user to set themselves new goals.
 */
public class SetGoals {
    public Button confirm;

    @FXML
    private TextField weightGoal, distanceGoal;

    private User currentUser;

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    /**
     * Method to take the users input and update the CSV files and give the user confirmation
     *
     * @throws IOException
     */
    public void setGoals() throws IOException {
        String weightGoalText = weightGoal.getText();
        String distanceGoalText = distanceGoal.getText();
        if (!weightGoalText.matches("[0-9]{1,}")) {
            invalidDataType("Ideal weight");
        } else if (!distanceGoalText.matches("[0-9]{1,}")) {
            invalidDataType("Distance goal");
        } else {
            currentUser.setDistanceGoal(Double.parseDouble(weightGoalText));
            currentUser.setWeightGoal(Double.parseDouble(weightGoalText));
            /**
             * need to save these updates to the file userdb.csv
             */
            currentUser.saveNewUserDetails();
            confirmationMessage();
        }
    }

    /**
     * Method to alert the user that their data is incorrect.
     */
    public void invalidDataType(String data) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Kenko");
        alert.setHeaderText("Invalid " + data);
        alert.setContentText("Please try again" + "\n" + "If you don't have a secondary goal please enter a 0.");
        alert.showAndWait();
    }

    /**
     * Method to confirm to the user that their data has been updated.
     */
    public void confirmationMessage() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Kenko");
        alert.setHeaderText("Successfully updated");
        alert.setContentText("Your new goal had been set!");
        alert.showAndWait();
    }

    public void openMenu(ActionEvent event) throws IOException{
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("menu.fxml"));
        Parent parent = loader.load();
        Menu controller = loader.getController();
        controller.setCurrentUser(currentUser);
        Scene scene = new Scene(parent);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }
}
