package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.chart.NumberAxis;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.ProgressBar;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Controller containing methods to display user progress in various ways such as through bars and graphs.
 */
public class Progress {

    @FXML
    private ProgressBar weightProgressBar, distanceProgressBar;

    private User currentUser;

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }
    /**
     * Method to display the user's weight progression in the form of a graph
     */



    public void displayWeightGraph() {
        Stage stage = new Stage();
        stage.setTitle("Kenko");
        //defining the axes
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("Weeks");
        yAxis.setLabel("Weight");
        //creating the chart
        final LineChart<Number, Number> lineChart = new LineChart<Number, Number>(xAxis, yAxis);
        lineChart.setTitle("Weight Progression");

        //defining a series
        XYChart.Series series = new XYChart.Series();
        try {
            currentUser.loadWeightProgress();
            for (int i = 0; i < currentUser.getWeightProgression().size(); i++) {
                double weight = currentUser.getWeightProgression().get(i);
                series.getData().add(new XYChart.Data(i, weight));
            }
            Scene scene = new Scene(lineChart, 800, 600);
            lineChart.getData().add(series);

            stage.setScene(scene);
            stage.show();
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
        lineChart.getData().add(series);
        stage.show();
    }

    /**
     * Method to display the distance graph (tracking the user's distance progress)
     */
    public void displayGraphDistance() {
        Stage stage = new Stage();
        stage.setTitle("Kenko");


        //defining the axes
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("Weeks");
        yAxis.setLabel("Distance");

        //creating the chart
        final LineChart<Number, Number> lineChart = new LineChart<Number, Number>(xAxis, yAxis);
        lineChart.setTitle("Distance Progression");

        //defining a series
        XYChart.Series series = new XYChart.Series();
        try {
            currentUser.loadDistanceProgress();
            for (int i = 0; i < currentUser.getDistanceProgression().size(); i++) {
                double distance = currentUser.getDistanceProgression().get(i);
                series.getData().add(new XYChart.Data(i, distance));
            }
            Scene scene = new Scene(lineChart, 800, 600);
            lineChart.getData().add(series);

            stage.setScene(scene);
            stage.show();
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
        lineChart.getData().add(series);
        stage.show();
    }

    /**
     * Method to update both of the progress bars with how close to user's goal weight and distance they are.
     */
    public void updateProgressBars() {
        currentUser.loadDistanceProgress();
        currentUser.loadWeightProgress();
        if (currentUser.achievedWeightGoalGoal())
        {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Kenko");
            alert.setHeaderText("YOU DID IT");
            alert.setContentText("CONGRATULATIONS! You have completed the weight goal you set for yourself. If you'd " +
                    "like to continue your journey with Kenko, please set yourself a new goal and we'll help you" +
                    "reach it!");
            alert.showAndWait();
        }
        else{
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Kenko");
            alert.setHeaderText("ALMOST THERE");
            alert.setContentText("Way to go! You're doing well on your weight journey, you've got a little way to go but keep " +
                    "up the good work brave warrior!");
            alert.showAndWait();
        }
        if (currentUser.achievedDistanceGoal())
        {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Kenko");
            alert.setHeaderText("YOU DID IT");
            alert.setContentText("CONGRATULATIONS! You have completed the distance goal you set for yourself. If you'd " +
                    "like to continue your journey with Kenko, please set yourself a new goal and we'll help you" +
                    "reach it!");
            alert.showAndWait();
        }
        else{
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Kenko");
            alert.setHeaderText("ALMOST THERE");
            alert.setContentText("Way to go! You're doing well on your distance journey, you've got a little way to go but keep " +
                    "up the good work brave warrior!");
            alert.showAndWait();
        }
        distanceProgressBar.setProgress(distanceProgressValue());
        weightProgressBar.setProgress(weightProgressValue());
    }


    /**
     * Method to calculate how close the user is to hitting their goal weight using goal & current weight
     * @return weightProgressValue (as a decimal)
     */
    public double weightProgressValue() {
        double goalWeight = currentUser.getWeightGoal();
        double currentWeight = currentUser.getMostRecentWeight();
        if (currentWeight > goalWeight)
        {
            return (goalWeight / currentWeight);
        }
        else {
            return (currentWeight / goalWeight);
        }
    }

    /**
     * Method to calculate how close the user is to hitting their goal distance using goal & current weight
     * @return distanceProgressValue (as a decimal)
     */
    public double distanceProgressValue() {
        currentUser.loadWeightProgress();
        currentUser.loadDistanceProgress();
        return currentUser.getMostRecentDistance() / currentUser.getDistanceGoal();
    }


    /**
     * A method to display an alert to the user showing their BMI value along with the ranges for which category
     * they fall in and the ranges.
     */
    public void displayBMI()
    {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Kenko");
        String bmiCat = "";
        double bmiVal = currentUser.BMI();
        if (bmiVal < 18.5)
        {
            bmiCat = "Underweight";
        }
        else if (18.5 < bmiVal && bmiVal < 24.9)
        {
            bmiCat = "healthy";
        }
        else if (25 < bmiVal && bmiVal < 29.9)
        {
            bmiCat = "overweight";
        }
        else{
            bmiCat = "obese";
        }
        alert.setHeaderText("Your BMI is: " + bmiVal + " this means that you are: " + bmiCat);
        alert.setContentText("Here are the BMI ranges: \n" +
                "below 18.5 – you're in the underweight range.\n" +
                "between 18.5 and 24.9 – you're in the healthy weight range.\n" +
                "between 25 and 29.9 – you're in the overweight range.\n" +
                "between 30 and 39.9 – you're in the obese range.");
        alert.showAndWait();
    }
    public void openMenu(ActionEvent event) throws IOException{
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("menu.fxml"));
        Parent parent = loader.load();
        Menu controller = loader.getController();
        controller.setCurrentUser(currentUser);
        Scene scene = new Scene(parent);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

}
