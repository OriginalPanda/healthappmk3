package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;

/**
 * Controller containing functions to change the view for the user to all the progression related items
 */
public class ProgressMenu {
    User currentUser;

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    /**
     * Method to open the progress scene
     * @throws IOException
     */

    public void openProgress(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("Progress.fxml"));
        Parent parent = loader.load();
        Progress controller = loader.getController();
        controller.setCurrentUser(currentUser);
        changeScene(event, parent);
    }

    /**
     * Method to open the set goals scene
     * @throws IOException
     */
    public void openSetGoals(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("setGoals.fxml"));
        Parent parent = loader.load();
        SetGoals controller = loader.getController();
        controller.setCurrentUser(currentUser);
        changeScene(event, parent);

    }

    /**
     * Method to open the update progress scene
     * @throws IOException
     */
    public void openUpdateProgress(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("updateProgress.fxml"));
        Parent parent = loader.load();
        UpdateProgress controller = loader.getController();
        controller.setCurrentUser(currentUser);
        changeScene(event, parent);
    }

    /**
     * This method is used to open the menu page from the progression menu
     * @param event
     * @throws IOException
     */
    public void openMenu(ActionEvent event) throws IOException{
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("menu.fxml"));
        Parent parent = loader.load();
        Menu controller = loader.getController();
        controller.setCurrentUser(currentUser);
        Scene scene = new Scene(parent);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    /**
     * This method is used to take the user to the share page where they can share their progression with freinds
     * @param event
     * @throws IOException
     */
    public void openShare(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("shareProgress.fxml"));
        Parent parent = loader.load();
        ShareProgress controller = loader.getController();
        controller.setCurrentUser(currentUser);
        changeScene(event, parent);

    }


    public void changeScene(ActionEvent event, Parent parent){
        Scene scene = new Scene(parent);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }
}
