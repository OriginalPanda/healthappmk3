/**
 * I need this for it to work on my PC feel free to comment out if it makes it stop working for you guys.
 *  - Jan
 */

module HealthAppMK2 {
    requires javafx.fxml;
    requires javafx.controls;
    requires java.datatransfer;
    requires java.desktop;
    requires java.mail;
    opens sample;
}